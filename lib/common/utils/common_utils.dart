import 'package:base_project/common/config/config.dart';
import 'package:base_project/common/utils/log_utils.dart';
import 'package:base_project/res/localization/default_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../global/global_controller.dart';
import '../../res/colors.dart';

/**
 * 通用逻辑
 */
typedef StringList = List<String>;

class CommonUtils {
  static Locale? curLocale;

  ///TODO: 更新主题用的
  static pushTheme(int index) {
    var globalController = GetInstance().find<GlobalController>();
    ThemeData themeData;
    List<Color> colors = getThemeListColor();
    themeData = getThemeData(colors[index]);

    globalController.state.themeData = themeData;
    globalController.update();
    // store.dispatch(new RefreshThemeDataAction(themeData));
  }

  static getThemeData(Color color) {
    return ThemeData(
      primarySwatch: color as MaterialColor?,
      platform: TargetPlatform.android,
      appBarTheme: AppBarTheme(
        systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
          systemNavigationBarContrastEnforced: true,
          systemStatusBarContrastEnforced: true,
          systemNavigationBarColor: color,
          statusBarColor: color,
          systemNavigationBarDividerColor: color.withAlpha(199),
        ),
      ),
    );
  }

  static List<Color> getThemeListColor() {
    return [
      ColorStyle.primarySwatch,
      Colors.brown,
      Colors.blue,
      Colors.teal,
      Colors.amber,
      Colors.blueGrey,
      Colors.deepOrange,
    ];
  }

  /**
   * 切换语言
   */
  static changeLocale(int index) {
    var globalController = GetInstance().find<GlobalController>();
    Locale? locale = globalController.state.platformLocale;
    if (Config.DEBUG!) {
      logObject(globalController.state.platformLocale);
    }

    ///TODO：应用内切换语言
    switch (index) {
      case 1:
        locale = Locale('zh', 'CH');
        break;
      case 2:
        locale = Locale('en', 'US');
        break;
    }
    curLocale = locale;
    globalController.update();
    // store.dispatch(RefreshLocaleAction(locale));
  }

  static const IMAGE_END = [".png", ".jpg", ".jpeg", ".gif", ".svg"];

  static isImageEnd(path) {
    bool image = false;
    for (String item in IMAGE_END) {
      if (path.indexOf(item) + item.length == path.length) {
        image = true;
      }
    }
    return image;
  }

  static copy(String? data, BuildContext context) {
    if (data != null) {
      Clipboard.setData(new ClipboardData(text: data));
      Fluttertoast.showToast(
          msg: AppLocalizations.i18n(context)!.option_share_copy_success);
    }
  }
}
