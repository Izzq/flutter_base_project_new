import '../config/config.dart';

///TODO: 优化Log日志打印工具类
class Logger {
  bool isLog = Config.DEBUG;

  v(String text) {
    if (!isLog) return;
    print(text);
  }

  d(String text) {
    if (!isLog) return;
    print(text);
  }

  i(String text) {
    if (!isLog) return;
    print(text);
  }

  w(String text) {
    if (!isLog) return;
    print(text);
  }

  e(String text) {
    if (!isLog) return;
    print(text);
  }

  wtf(String text) {
    if (!isLog) return;
    print(text);
  }

  logException(Object? object) {
    if (!isLog) return;
    print(object);
  }

  logObject(Object? object) {
    if (!isLog) return;
    print(object);
  }
}

var _logger = Logger();

LogV(String msg) {
  _logger.v(msg);
}

LogD(String msg) {
  _logger.d(msg);
}

LogI(String msg) {
  _logger.i(msg);
}

LogW(String msg) {
  _logger.w(msg);
}

LogE(String msg) {
  _logger.e(msg);
}

LogWTF(String msg) {
  _logger.wtf(msg);
}

logException(Object? object) {
  _logger.logException(object);
}

logObject(Object? object) {
  _logger.logObject(object);
}
