import 'package:base_project/common/event/http_error_event.dart';
import 'package:base_project/common/event/index.dart';

///错误编码
class Code {
  static const SUCCESS = 200;

  ///网络错误
  static const NETWORK_ERROR = -1;

  ///网络超时
  static const NETWORK_TIMEOUT = -2;

  ///网络返回数据格式化异常
  static const NETWORK_JSON_EXCEPTION = -3;

  static errorHandleFunction(code, message, noTip) {
    if (noTip) {
      return message;
    }
    eventBus.fire(new HttpErrorEvent(code, message));
    return message;
  }
}
