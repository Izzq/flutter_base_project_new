import 'package:base_project/common/config/config.dart';
import 'package:base_project/common/utils/sp_utils.dart';
import 'package:dio/dio.dart';

import '../../utils/log_utils.dart';

/**
 * Token拦截器
 */
class TokenInterceptors extends InterceptorsWrapper {
  ///TODO：自行修改处理登录状态token逻辑
  String? _token;

  @override
  onRequest(RequestOptions options, handler) async {
    //授权码
    if (_token == null) {
      var authorizationCode = await getAuthorization();
      if (authorizationCode != null) {
        _token = authorizationCode;
      }
    }
    if (_token != null) {
      options.headers["Authorization"] = _token;
    }
    return super.onRequest(options, handler);
  }

  @override
  onResponse(Response response, handler) async {
    try {
      var responseJson = response.data;
      if (response.statusCode == 201 && responseJson["token"] != null) {
        _token = 'token ' + responseJson["token"];
        await SPUtils.save(Config.TOKEN_KEY, _token);
      }
    } catch (e) {
      logException(e);
    }
    return super.onResponse(response, handler);
  }

  ///清除授权
  clearAuthorization() {
    this._token = null;
    SPUtils.remove(Config.TOKEN_KEY);
  }

  ///获取授权token
  getAuthorization() async {
    String? token = await SPUtils.get(Config.TOKEN_KEY);
    if (token == null) {
    } else {
      this._token = token;
      return token;
    }
  }
}
