import 'package:base_project/common/net/code.dart';
import 'package:base_project/common/net/result_data.dart';
import 'package:dio/dio.dart';

import '../../utils/log_utils.dart';

/**
 * Token拦截器
 */
class ResponseInterceptors extends InterceptorsWrapper {
  @override
  onResponse(Response response, handler) async {
    RequestOptions option = response.requestOptions;
    var value;
    try {
      var header = response.headers[Headers.contentTypeHeader];

      ///TODO:此处处理问题，为何网络请求判断 header.toString().contains("text") 为也成功状态？
      if ((header != null && header.toString().contains("text"))) {
        value = new ResultData(response.data, true, Code.SUCCESS);
      } else if (response.statusCode! >= 200 && response.statusCode! < 300) {
        value = new ResultData(response.data, true, Code.SUCCESS,
            headers: response.headers);
      }
    } catch (e) {
      LogD(e.toString() + option.path);
      value = new ResultData(response.data, false, response.statusCode,
          headers: response.headers);
    }
    response.data = value;
    return handler.next(response);
  }
}
