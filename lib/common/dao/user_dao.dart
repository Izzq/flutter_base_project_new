import 'package:base_project/common/dao/dao_result.dart';

class UserDao {
  static oauth(code, store) async {
    return new DataResult("resultData", false);
  }

  static login(userName, password, store) async {
    return new DataResult("", false);
  }

  ///获取本地登录用户信息
  static getUserInfoLocal() async {
    return new DataResult(null, false);
  }

  ///获取用户详细信息
  static getUserInfo(userName, {needDb = false}) async {
    return null;
  }

  static clearAll() async {}
}
