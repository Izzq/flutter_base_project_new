///数据库数据结果
class DataResult {
  var data;
  bool result;
  Function? next;

  DataResult(this.data, this.result, {this.next});
}
