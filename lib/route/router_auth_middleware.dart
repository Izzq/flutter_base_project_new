import 'package:base_project/route/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/routes/route_middleware.dart';

///登陆拦截中间件
class RouteAuthMiddleware extends GetMiddleware {
  @override
  int? priority = 0;

  RouteAuthMiddleware({required this.priority});

  //TODO: 如何添加此路由（登陆拦截中间件）拦截逻辑？
  @override
  RouteSettings? redirect(String? route) {
    // Future.delayed(
    //     const Duration(seconds: 1), () => Get.snackbar("提示", "请先登录APP"));
    // return const RouteSettings(name: AppRoutes.TEST_PAGE);
    return const RouteSettings(name: AppRoutes.mainPage);
  }
}
