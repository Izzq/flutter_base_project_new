import 'package:base_project/page/home/home_binding.dart';
import 'package:base_project/page/home/home_view.dart';
import 'package:base_project/page/main/main_binding.dart';
import 'package:base_project/page/main/main_view.dart';
import 'package:base_project/page/mine/mine_binding.dart';
import 'package:base_project/page/mine/mine_view.dart';
import 'package:base_project/page/sites/sites_binding.dart';
import 'package:base_project/page/sites/sites_view.dart';
import 'package:get/get.dart';

import '../page/nodes/nodes_binding.dart';
import '../page/nodes/nodes_view.dart';
import '../page/test/test_binding.dart';
import '../page/test/test_view.dart';

abstract class AppRoutes {
  static const testPage = "/TestPage"; //测试页
  static const mainPage = "/main_page"; //主页Main
  static const homePage = "/home_page"; //首页Tab
  static const sitesPage = "/sites_page"; //sites Tab
  static const minePage = "/mine_page"; //我的Tab
  static const nodesPage = "/nodes_page"; //节点列表
  static const loginPage = "/login_page"; //节点列表

  static final routerPages = [
    ///测试页
    GetPage(
        name: AppRoutes.testPage,
        page: () => const TestPage(),
        binding: TestBinding()),

    ///主入口
    GetPage(
        name: AppRoutes.mainPage,
        page: () => const MainPage(),
        binding: MainBinding()),

    ///首页Tab
    GetPage(
      name: AppRoutes.homePage,
      page: () => const HomePage(),
      binding: HomeBinding(),
    ),

    ///Sites Tab
    GetPage(
      name: AppRoutes.sitesPage,
      page: () => const SitesPage(),
      binding: SitesBinding(),
    ),

    ///我的Tab
    GetPage(
        name: AppRoutes.minePage,
        page: () => const MinePage(),
        binding: MineBinding()),

    ///节点列表
    GetPage(
        name: AppRoutes.nodesPage,
        page: () => const NodesPage(),
        binding: NodesBinding()),
  ];
}
