import 'package:base_project/route/routes.dart';
import 'package:get/get.dart';

/// 统一页面路由跳转管理
class RouterUtils {
  ///打开测试页
  static goTestPage() {
    Get.toNamed(AppRoutes.testPage);
  }

  ///打开主页
  static goMainPage() {
    Get.toNamed(AppRoutes.mainPage);
  }

  ///打开节点列表页
  static goNodesPage() {
    Get.toNamed(AppRoutes.nodesPage);
  }
}
