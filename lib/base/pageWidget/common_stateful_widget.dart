import 'package:base_project/mixin/toast/toast_mixin.dart';
import 'package:flutter/material.dart';

///实现一些基础功能StatefulWidget组件（例如Toast等，可继续拓展）
abstract class CommonStatefulWidget extends StatefulWidget with ToastMixin {}
