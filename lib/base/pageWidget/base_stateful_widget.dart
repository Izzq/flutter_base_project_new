import 'package:base_project/base/controller/base_controller.dart';
import 'package:base_project/common/utils/log_utils.dart';
import 'package:base_project/mixin/toast/toast_mixin.dart';
import 'package:base_project/widget/load_state_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widget/loading_widget.dart';

///具有状态管理的基础页面，满足一些特定需要State的Widget(暂时还未发现需要使用State的场景)
abstract class BaseStatefulWidget<T extends BaseController, M>
    extends StatefulWidget with ToastMixin {
  const BaseStatefulWidget({Key? key}) : super(key: key);

  final String? tag = null;

  ///控制器，相当于P层（逻辑层）
  T get controller {
    return GetInstance().find<T>(tag: tag);
  }

  ///状态信息，相当于P层（逻辑层）变量
  M get state {
    return GetInstance().find<T>(tag: tag).state;
  }

  ///Get 局部更新字段
  get updateId => null;

  ///这是Widget的核心方法，用于构建Widget的UI。在初始化Widget和在需要重建Widget时都会调用此方法。
  ///当调用setState方法时，会触发Widget的重建，从而调用build方法。
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _createAppBar(context),
      body: buildBody(context),
      drawer: showDrawer() ? createDrawer() : null,
    );
  }

  ///AppBar生成逻辑
  AppBar? _createAppBar(BuildContext context) {
    if (showTitleBar()) {
      return createAppBar(
          titleString(), showBackButton(), appBarActionWidget(context),
          titleWidget: titleWidget());
    } else {
      return null;
    }
  }

  ///构建侧边栏内容
  Widget createDrawer() {
    return Container();
  }

  ///创建AppBar ActionView
  List<Widget>? appBarActionWidget(BuildContext context) {}

  ///构建Scaffold-body主体内容
  Widget buildBody(BuildContext context) {
    if (useLoadSir()) {
      return controller.obx((state) => buildContent(context),
          onLoading: Center(
            child: LoadingWidget(),
          ),
          onError: (error) => createErrorWidget(controller, error),
          onEmpty: createEmptyWidget(controller));
    } else {
      return buildContent(context);
    }
  }

  ///是否展示titleBar标题栏
  bool showTitleBar() => true;

  ///侧边栏
  bool showDrawer() => false;

  ///页面标题设置
  String titleString() => "Title";

  ///标题栏title的Widget
  Widget? titleWidget() {}

  ///是否开启加载状态
  bool useLoadSir() => true;

  ///是否展示回退按钮
  bool showBackButton() => true;

  ///showSuccess展示成功的布局
  Widget buildContent(BuildContext context);

  ///widget生命周期
  get lifecycle => null;

  ///当创建一个StatefulWidget时，该方法会被调用，用于创建与该Widget关联的状态对象。
  @override
  State createState() {
    LogD("[${this}] >>>>>>>>>>>>createState <==> onCreate（Android）");
    return AutoDisposeState<T>();
  }
}

class AutoDisposeState<T extends GetxController>
    extends State<BaseStatefulWidget>
    with
        AutomaticKeepAliveClientMixin<BaseStatefulWidget>,
        WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GetBuilder<T>(
        tag: widget.tag,
        id: widget.updateId,
        builder: (controller) {
          return widget.build(context);
        });
  }

  ///在State对象被创建时调用，通常用于初始化操作，如订阅事件或初始化状态。
  @override
  void initState() {
    super.initState();
    LogD("[${this}] >>>>>>>>>>>>initState <==> onStart（Android）");
    if (widget.lifecycle != null) {
      WidgetsBinding.instance?.addObserver(this);
    }
  }

  @override
  void deactivate() {
    super.deactivate();
    LogD("[${this}] >>>>>>>>>>>>deactivate");
  }

  @override
  void dispose() {
    Get.delete<T>(tag: widget.tag);
    if (widget.lifecycle != null) {
      WidgetsBinding.instance?.removeObserver(this);
    }
    super.dispose();
    LogD("[${this}] >>>>>>>>>>>>dispose <==> onDestroy（Android）");
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (widget.lifecycle != null) {
      widget.lifecycle(state);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
