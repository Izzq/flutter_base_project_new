import 'package:base_project/common/utils/log_utils.dart';
import 'package:base_project/mixin/toast/toast_mixin.dart';
import 'package:get/get.dart';

///TODO: 注意：BaseController 此处生命周期意义跟Android有所不同
///具有状态控制和网络请求能力的controller，等价MVVM中的ViewModel
abstract class BaseController extends SuperController with ToastMixin {
  void loadNet() {}

  @override
  void onDetached() {
    LogD('[$this] >>>>>>>onDetached');
  }

  @override
  void onInactive() {
    LogD('[$this] >>>>>>>onInactive');
  }

  @override
  void onPaused() {
    LogD('[$this] >>>>>>>onPaused');
  }

  @override
  void onResumed() {
    LogD('[$this] >>>>>>>onResumed');
  }

  @override
  void onHidden() {
    LogD('[$this] >>>>>>>onHidden');
  }

  @override
  void onReady() {
    super.onReady();
    LogD('[$this] >>>>>>>onReady <<==>> Controller 初始化');
    showSuccess();
  }

  @override
  void onClose() {
    super.onClose();
    LogD('[$this] >>>>>>>onClose <<==>> Controller 销毁');
  }

  ///显示页面内容
  void showSuccess() {
    change(null, status: RxStatus.success());
  }

  ///显示空白内容
  void showEmpty() {
    change(null, status: RxStatus.empty());
  }

  ///显示加载内容
  void showLoading() {
    change(null, status: RxStatus.loading());
  }
}
