import 'dart:async';

import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'enums.dart';
import 'method_channel_flutter.dart';

/// 连接性的实现必须实现的接口。
/**
 * 平台实现应该扩展这个类，而不是实现它，因为“Connectivity”不认为新添加的方法是破坏性的更改。
 * 扩展这个类(使用' extends ')确保子类将获得默认实现，而'实现'这个接口的平台实现将被新添加
 * 的[FlutterPlatform]方法破坏。
 */
abstract class FlutterPlatform extends PlatformInterface {
  /// Constructs a FlutterPlatform.
  FlutterPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterPlatform _instance = MethodChannelFlutter();

  /// 使用的[FlutterPlatform]的默认实例。
  ///
  /// 默认为[MethodChannelFlutter]。
  static FlutterPlatform get instance => _instance;

  /// 特定于平台的插件应该在注册时使用自己的特定于平台的类来扩展[FlutterPlatform]。
  static set instance(FlutterPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  /// 检查设备的连接状态。
  Future<FlutterResult> checkConnectivity() {
    throw UnimplementedError('checkConnectivity() has not been implemented.');
  }

  /// 返回FlutterResult更改流。
  Stream<FlutterResult> get onConnectivityChanged {
    throw UnimplementedError(
        'get onConnectivityChanged has not been implemented.');
  }
}
