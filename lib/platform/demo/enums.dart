/// 连接状态检查结果。
enum FlutterResult {
  /// Bluetooth: 设备通过蓝牙连接
  bluetooth,

  /// WiFi: 通过Wi-Fi连接的设备
  wifi,

  /// Ethernet: 设备连接到以太网网络
  ethernet,

  /// Mobile: 设备连接到蜂窝网络
  mobile,

  /// None: 设备未连接到任何网络
  none,

  /// VPN:连接到VPN的设备
  ///
  /// Note for iOS and macOS:
  /// [vpn]没有单独的网络接口类型。它在任何设备(也包括模拟器)上返回[other]。
  vpn,

  /// Other: 设备连接到未知网络
  other
}
