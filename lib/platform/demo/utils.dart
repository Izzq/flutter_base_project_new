import 'enums.dart';

/// 将字符串转换为ConnectivityResult值。
FlutterResult parseConnectivityResult(String state) {
  switch (state) {
    case 'bluetooth':
      return FlutterResult.bluetooth;
    case 'wifi':
      return FlutterResult.wifi;
    case 'ethernet':
      return FlutterResult.ethernet;
    case 'mobile':
      return FlutterResult.mobile;
    case 'vpn':
      return FlutterResult.vpn;
    case 'other':
      return FlutterResult.other;
    case 'none':
    default:
      return FlutterResult.none;
  }
}
