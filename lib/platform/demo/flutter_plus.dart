import 'dart:async';

import 'enums.dart';
import 'flutter_plus_platform_interface.dart';

/// 发现网络连接配置:区分WI-FI和蜂窝，检查WI-FI状态和更多。
class FlutterBridgeImpl {
  /// 构造一个[FlutterBridgeImpl]的单例实例。
  ///
  /// [FlutterBridgeImpl]被设计为单例工作。
  // 当创建第二个实例时，第一个实例将无法侦听EventChannel，因为它被覆盖了。将类强制为单例类可以防止程序员误用创建第二个实例。
  factory FlutterBridgeImpl() {
    _singleton ??= FlutterBridgeImpl._();
    return _singleton!;
  }

  FlutterBridgeImpl._();

  static FlutterBridgeImpl? _singleton;

  static FlutterPlatform get _platform {
    return FlutterPlatform.instance;
  }

  /// 每当连接状态改变时触发。
  ///
  /// 在iOS上，当WiFi状态改变时，连接状态可能不会更新，这是一个只影响模拟器的已知问题。
  /// For details see https://github.com/fluttercommunity/plus_plugins/issues/479.
  Stream<FlutterResult> get onConnectivityChanged {
    return _platform.onConnectivityChanged;
  }

  /// 检查设备的连接状态。
  ///
  /// 不要用这个函数的结果来决定是否可以可靠地发出网络请求。它只给你无线电状态。
  ///
  /// 相反，通过[onConnectivityChanged]流侦听连接变化。
  Future<FlutterResult> checkConnectivity() {
    return _platform.checkConnectivity();
  }
}
