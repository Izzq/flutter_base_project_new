class PlatformConst {
  static const String MethodChannel = "com.won.project/MethodChannel";
  static const String EventChannel = "com.won.project/EventChannel";

  ///////////////////////////////////////////////////////
  static const String ACTION_VPN_CONNECT = "action_vpn_connect";
  static const String ACTION_VPN_DISCONNECT = "action_vpn_disconnect";
  static const String ACTION_VPN_SWITCH_CONNECT = "action_vpn_switch_connect";

  ///////////////////////////////////////////////////////
  static const String UPDATE_VPN_STATE = "update_vpn_state";
}
