import 'package:base_project/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///通用样式提取

///文本样式
class Styles {
  static TextStyle style_white_24 =
      TextStyle(color: Colors.white, fontSize: 24.sp);

  static TextStyle style_6A6969_28 =
      TextStyle(color: ColorStyle.color_6A6969, fontSize: 28.sp);

  static TextStyle style_black_32 =
      TextStyle(color: Colors.black, fontSize: 32.sp);

  static TextStyle style_black_30_bold500 = TextStyle(
      color: Colors.black, fontSize: 30.sp, fontWeight: FontWeight.w500);

  static TextStyle style_white_32_bold = TextStyle(
      color: Colors.white, fontSize: 32.sp, fontWeight: FontWeight.bold);

  /////////////////////////////////////////////////////////////////////////

  static const lagerTextSize = 30.0;
  static const bigTextSize = 23.0;
  static const normalTextSize = 18.0;
  static const middleTextWhiteSize = 16.0;
  static const smallTextSize = 14.0;
  static const minTextSize = 12.0;

  static const minText = TextStyle(
    color: ColorStyle.subLightTextColor,
    fontSize: minTextSize,
  );

  static const smallTextWhite = TextStyle(
    color: ColorStyle.textColorWhite,
    fontSize: smallTextSize,
  );

  static const smallText = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: smallTextSize,
  );

  static const smallTextBold = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: smallTextSize,
    fontWeight: FontWeight.bold,
  );

  static const smallSubLightText = TextStyle(
    color: ColorStyle.subLightTextColor,
    fontSize: smallTextSize,
  );

  static const smallActionLightText = TextStyle(
    color: ColorStyle.actionBlue,
    fontSize: smallTextSize,
  );

  static const smallMiLightText = TextStyle(
    color: ColorStyle.miWhite,
    fontSize: smallTextSize,
  );

  static const smallSubText = TextStyle(
    color: ColorStyle.subTextColor,
    fontSize: smallTextSize,
  );

  static const middleText = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: middleTextWhiteSize,
  );

  static const middleTextWhite = TextStyle(
    color: ColorStyle.textColorWhite,
    fontSize: middleTextWhiteSize,
  );

  static const middleSubText = TextStyle(
    color: ColorStyle.subTextColor,
    fontSize: middleTextWhiteSize,
  );

  static const middleSubLightText = TextStyle(
    color: ColorStyle.subLightTextColor,
    fontSize: middleTextWhiteSize,
  );

  static const middleTextBold = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: middleTextWhiteSize,
    fontWeight: FontWeight.bold,
  );

  static const middleTextWhiteBold = TextStyle(
    color: ColorStyle.textColorWhite,
    fontSize: middleTextWhiteSize,
    fontWeight: FontWeight.bold,
  );

  static const middleSubTextBold = TextStyle(
    color: ColorStyle.subTextColor,
    fontSize: middleTextWhiteSize,
    fontWeight: FontWeight.bold,
  );

  static const normalText = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: normalTextSize,
  );

  static const normalTextBold = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: normalTextSize,
    fontWeight: FontWeight.bold,
  );

  static const normalSubText = TextStyle(
    color: ColorStyle.subTextColor,
    fontSize: normalTextSize,
  );

  static const normalTextWhite = TextStyle(
    color: ColorStyle.textColorWhite,
    fontSize: normalTextSize,
  );

  static const normalTextMitWhiteBold = TextStyle(
    color: ColorStyle.miWhite,
    fontSize: normalTextSize,
    fontWeight: FontWeight.bold,
  );

  static const normalTextActionWhiteBold = TextStyle(
    color: ColorStyle.actionBlue,
    fontSize: normalTextSize,
    fontWeight: FontWeight.bold,
  );

  static const normalTextLight = TextStyle(
    color: ColorStyle.primaryLightValue,
    fontSize: normalTextSize,
  );

  static const largeText = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: bigTextSize,
  );

  static const largeTextBold = TextStyle(
    color: ColorStyle.mainTextColor,
    fontSize: bigTextSize,
    fontWeight: FontWeight.bold,
  );

  static const largeTextWhite = TextStyle(
    color: ColorStyle.textColorWhite,
    fontSize: bigTextSize,
  );

  static const largeTextWhiteBold = TextStyle(
    color: ColorStyle.textColorWhite,
    fontSize: bigTextSize,
    fontWeight: FontWeight.bold,
  );

  static const largeLargeTextWhite = TextStyle(
    color: ColorStyle.textColorWhite,
    fontSize: lagerTextSize,
    fontWeight: FontWeight.bold,
  );

  static const largeLargeText = TextStyle(
    color: ColorStyle.primaryValue,
    fontSize: lagerTextSize,
    fontWeight: FontWeight.bold,
  );
}

class DividerStyle {
  ///分割线 0.5 - 20边距
  static Widget divider1HalfPadding20 = Divider(
    height: 0.5.w,
    thickness: 0.5,
    indent: 20,
    endIndent: 20,
    color: ColorStyle.colorShadow,
  );

  ///分割线 0.5 - 20边距
  static Widget divider2Padding20 = Divider(
    height: 2.w,
    indent: 20,
    endIndent: 20,
    color: ColorStyle.colorShadow2,
  );

  ///分割线 0.5 - 无边距
  static Widget divider1Half = Divider(
    height: 1.w,
    thickness: 0.5,
    color: ColorStyle.colorShadow,
  );

  ///分割线 20 - 无边距
  static Widget divider20Half = const Divider(
    height: 20,
    thickness: 20,
    color: ColorStyle.color_F8F9FC,
  );
}

/// 间隔
class Box {
  /// 水平间隔
  static Widget wBox30 = SizedBox(width: 30.w);
  static Widget hBox20 = SizedBox(height: 20.w);
  static Widget hBox30 = SizedBox(height: 30.w);
}
