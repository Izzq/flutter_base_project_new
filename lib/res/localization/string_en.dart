import 'package:base_project/res/localization/string_base.dart';

class AppStringEn extends AppStringBase {
  @override
  String app_name = "应用名称";
  @override
  String app_ok = "ok";
  @override
  String app_cancel = "cancel";
  @override
  String app_empty = "Empty(oﾟ▽ﾟ)o";

  @override
  String option_share_copy_success = "Copy Success";
  @override
  String network_error_timeout = "Http timeout";
  @override
  String network_error_unknown = "Http unknown error";
  @override
  String network_error = "network error";
}
