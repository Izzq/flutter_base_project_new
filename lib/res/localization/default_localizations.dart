import 'package:base_project/res/localization/string_base.dart';
import 'package:base_project/res/localization/string_en.dart';
import 'package:base_project/res/localization/string_zh.dart';
import 'package:flutter/material.dart';

///自定义多语言实现
class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  ///根据不同 locale.languageCode 加载不同语言对应
  ///AppStringEn 和 AppStringZh 都继承了 AppStringBase
  static Map<String, AppStringBase> _localizedValues = {
    'en': new AppStringEn(),
    'zh': new AppStringZh(),
  };

  AppStringBase? get currentLocalized {
    if (_localizedValues.containsKey(locale.languageCode)) {
      return _localizedValues[locale.languageCode];
    }
    return _localizedValues["en"];
  }

  ///通过 Localizations 加载当前的 AppLocalizations
  ///获取对应的 AppStringBase
  static AppLocalizations? of(BuildContext context) {
    return Localizations.of(context, AppLocalizations);
  }

  ///通过 Localizations 加载当前的 AppLocalizations
  ///获取对应的 AppStringBase
  static AppStringBase? i18n(BuildContext context) {
    return (Localizations.of(context, AppLocalizations) as AppLocalizations)
        .currentLocalized;
  }
}
