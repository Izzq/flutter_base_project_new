import 'package:base_project/res/localization/string_base.dart';

class AppStringZh extends AppStringBase {
  @override
  String app_name = "应用名称";
  @override
  String app_ok = "确定";
  @override
  String app_cancel = "取消";
  @override
  String app_empty = "目前什么也没有哟";

  @override
  String option_share_copy_success = "已经复制到粘贴板";

  @override
  String network_error_timeout = "请求超时";
  @override
  String network_error_unknown = "其他异常";
  @override
  String network_error = "网络错误";
}
