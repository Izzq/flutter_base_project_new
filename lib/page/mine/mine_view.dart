import 'package:base_project/base/pageWidget/base_stateful_widget.dart';
import 'package:base_project/page/mine/mine_state.dart';
import 'package:flutter/material.dart';

import 'mine_logic.dart';

class MinePage extends BaseStatefulWidget<MineLogic, MineState> {
  const MinePage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context) {
    return Container();
  }

  @override
  String titleString() => "我的";

  @override
  bool showBackButton() => false;
}
