## 项目目录结构

### 视图模块使用规范

page 目录下的内容主要为页面组件相关的。
子目录目录以模块名称来命名，如：home，子目录里面可能存在 widget 目录。
widget 目录：一般存放改模块相关的组件，如：自定义组件（View）、列表item（View）等。

### getX 使用规范

1、开发理念：flutter主张声明式开发理念，即不需要手动调用更新方法，只需要修改状态变量，视图随之做出对应的变化。

2、组件（视图、View）刷新：
2.1 flutter 刷新组件一般是用 saveState() 方法，此方式开销比较大，为全局刷新，下面推荐 getX 的刷新组件的几种方式。

2.2、组件（视图、View）刷新：getX组件刷新一般有三种方式（GetBuilder<Controller>、GetX<Controller>、Obx），
GetBuilder 需要配合 update()方法使用才会刷新组件状态，GetX<Controller>、Obx 两种方式效果是一样的，Obx更为简洁，推荐使用。






