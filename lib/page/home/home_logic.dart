import 'package:base_project/base/controller/base_controller.dart';

import 'home_state.dart';

class HomeLogic extends BaseController {
  final HomeState state = HomeState();
}
