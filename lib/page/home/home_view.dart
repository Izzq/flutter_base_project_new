import 'package:base_project/base/pageWidget/base_stateful_widget.dart';
import 'package:base_project/page/home/home_state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../global/global_controller.dart';
import 'home_logic.dart';

class HomePage extends BaseStatefulWidget<HomeLogic, HomeState> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context) {
    final globalController = Get.find<GlobalController>();

    return Container();
  }

  @override
  String titleString() => "首页";

  @override
  bool showBackButton() => false;

  ///搜索按钮
  @override
  List<Widget>? appBarActionWidget(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            //点击返回键动作
          },
          icon: const Icon(Icons.search))
    ];
  }
}
