import 'package:base_project/base/pageWidget/base_stateful_widget.dart';
import 'package:flutter/material.dart';

import 'sites_logic.dart';
import 'sites_state.dart';

class SitesPage extends BaseStatefulWidget<SitesLogic, SitesState> {
  const SitesPage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context) {
    return Container();
  }

  @override
  String titleString() => "我的";

  @override
  bool showBackButton() => false;

  ///搜索按钮
  @override
  List<Widget>? appBarActionWidget(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            //点击搜索动作
          },
          icon: const Icon(Icons.search))
    ];
  }
}
