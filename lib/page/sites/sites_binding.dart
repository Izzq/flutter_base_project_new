import 'package:get/get.dart';

import 'sites_logic.dart';

class SitesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SitesLogic());
  }
}
