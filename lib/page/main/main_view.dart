import 'package:base_project/base/pageWidget/base_stateful_widget.dart';
import 'package:base_project/page/main/main_state.dart';
import 'package:base_project/route/router_utils.dart';
import 'package:flutter/material.dart';

import 'main_logic.dart';

class MainPage extends BaseStatefulWidget<MainLogic, MainState> {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 50.0),
      child: Center(
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
            child: Column(
              children: <Widget>[
                ElevatedButton(
                  onPressed: () {
                    // 在按钮点击时执行的操作
                    RouterUtils.goNodesPage();
                  },
                  child: Text('Click Me to goNodesPage'),
                ),
                ElevatedButton(
                  onPressed: () {
                    // 在按钮点击时执行的操作
                    RouterUtils.goTestPage();
                  },
                  child: Text('Click Me to goTestPage'),
                ),
              ],
            )),
      ),
    );
  }

  @override
  String titleString() => "主页面";

  @override
  bool showTitleBar() => false;
}
