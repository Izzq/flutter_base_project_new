import 'package:get/get.dart';

import 'layut_logic.dart';


class LayoutBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LayoutLogic());
  }
}
