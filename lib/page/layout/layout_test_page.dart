import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../base/pageWidget/base_stateless_widget.dart';
import 'layut_logic.dart';

class MyWidget extends BaseStatelessWidget<LayoutLogic> {
  @override
  Widget buildContent(BuildContext context) {
    return _buildContent9(context);
  }

  /// 线性布局
  /// 这个Flutter布局使用了Container和Column来实现垂直排列的效果，SizedBox用于添加两个View之间的间距。
  /// 请注意，Flutter中使用颜色时，我们使用Colors类来表示颜色，而不是直接使用颜色代码。
  Widget _buildContent1(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 100.0,
            height: 100.0,
            margin: EdgeInsets.only(left: 10.0),
            color: Colors.black,
          ),
          SizedBox(height: 20.0), // 添加间距
          Container(
            width: 100.0,
            height: 100.0,
            margin: EdgeInsets.only(left: 10.0, top: 20.0),
            color: Colors.black,
          ),
        ],
      ),
    );
  }

  /// 相对布局 上下两左边
  ///在Flutter中，我们使用Stack和Positioned来实现相对布局的效果。Positioned用于指定子widget的位置。
  ///请注意，在Flutter中，间距通常使用逻辑像素（而不是dp）表示。
  Widget _buildContent2(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0.0),
      child: Stack(
        children: [
          Positioned(
            left: 10.0,
            top: 10.0,
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
          Positioned(
            left: 120.0,
            top: 120.0, // 20dp的间距在Flutter中以逻辑像素表示
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  /// 相对布局 均分
  Widget _buildContent5(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              width: 100.0,
              color: Colors.black,
            ),
          ),
          SizedBox(height: 10.0), // 间距
          Expanded(
            flex: 1,
            child: Container(
              width: 100.0,
              color: Colors.teal[200],
              margin: EdgeInsets.symmetric(vertical: 10.0),
            ),
          ),
          SizedBox(height: 10.0), // 间距
          Expanded(
            flex: 1,
            child: Container(
              width: 100.0,
              color: Colors.purple[200],
            ),
          ),
        ],
      ),
    );
  }

  /// 相对布局 上下、一左一右左边
  Widget _buildContent3(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Stack(
        children: [
          Positioned(
            top: 0.0,
            right: 10.0,
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
          Positioned(
            top: 120.0, // 20dp的间距在Flutter中以逻辑像素表示
            left: 10.0,
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  /// 相对布局 上下左右
  Widget _buildContent4(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Stack(
        children: [
          Positioned(
            left: 10.0,
            top: 10.0,
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
          Positioned(
            left: 120.0,
            // 100dp (view1 width) + 10dp (view1 margin) + 10dp (view5 margin)
            top: 10.0,
            child: Container(
              width: 40.0,
              height: 40.0,
              color: Colors.red,
            ),
          ),
          Positioned(
            right: 20.0,
            top: 10.0,
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
          Positioned(
            // below: 'view1',
            left: 10.0,
            top: 120.0,
            // 10dp (view1 margin) + 10dp (view3 margin) + 100dp (view1 height)
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
          Positioned(
            // below: 'view2',
            right: 20.0,
            top: 120.0,
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  /// 文字
  Widget _buildContent6(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: [
          Container(
            width: double.infinity, // 宽度占满父容器
            height: 120.0,
            // height: double.infinity,
            child: Container(
              width: 100.0,
              height: 120.0,
              color: Colors.teal[200],
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 19.0),
                  child: Text(
                    "阿事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是"
                    "撒阿事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是撒"
                    "事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是撒",
                    style: TextStyle(fontSize: 12.0),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: 100.0,
            height: 100.0,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  /// 设置最小高度，并根据文字内容扩充高度
  Widget _buildContent7(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: [
          Container(
            width: double.infinity, // 宽度占满父容器
            constraints: BoxConstraints(minHeight: 120.0), // 最小高度
            child: Container(
              width: 200.0,
              decoration: BoxDecoration(
                color: Colors.teal[200],
              ),
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 19.0),
                  child: Text(
                    "阿事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是"
                    "撒阿事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是撒"
                    "事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是撒"
                    "事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是撒"
                    "事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是撒"
                    "事实上事实上事实上是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒是撒少时诵诗书少时诵诗书是撒是撒是撒是撒是撒是撒是撒",
                    style: TextStyle(fontSize: 12.0),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// 设置最小高度，并根据内容扩充高度
  /// 这个Flutter布局使用了Container和BoxDecoration来设置容器的宽度和背景颜色，
  /// 并使用constraints属性来设置最小高度。在内部的Column中，使用SizedBox小部件来添加两个View之间的间距。
  /// 请注意，在Flutter中使用颜色时，我们使用Colors类来表示颜色，而不是直接使用颜色代码。
  Widget _buildContent8(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: [
          Container(
            width: 200.0,
            constraints: BoxConstraints(minHeight: 120.0), // 最小高度
            child: Container(
              decoration: BoxDecoration(
                color: Colors.teal[200],
              ),
              child: Column(
                children: [
                  Container(
                    width: 100.0,
                    height: 100.0,
                    margin: EdgeInsets.only(left: 10.0),
                    color: Colors.black,
                  ),
                  SizedBox(height: 20.0), // 间距
                  Container(
                    width: 100.0,
                    height: 100.0,
                    margin: EdgeInsets.only(left: 10.0),
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


  ///这个Flutter布局使用了Container和BoxDecoration来设置容器的宽度和背景颜色，
  ///并使用constraints属性来设置最小高度。在内部的Column中，使用crossAxisAlignment属性将子元素右对齐。
  Widget _buildContent9(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: [
          Container(
            width: double.infinity, // 宽度占满父容器
            constraints: BoxConstraints(minHeight: 120.0), // 最小高度
            child: Container(
              width: 200.0,
              decoration: BoxDecoration(
                color: Colors.teal[200],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end, // 设置子元素右对齐
                children: [
                  Container(
                    width: 100.0,
                    height: 100.0,
                    margin: EdgeInsets.only(left: 10.0),
                    color: Colors.black,
                  ),
                  SizedBox(height: 20.0), // 间距
                  Container(
                    width: 100.0,
                    height: 100.0,
                    margin: EdgeInsets.only(left: 10.0),
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


}
