import 'package:get/get.dart';

import 'nodes_logic.dart';

class NodesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NodesLogic());
  }
}
