import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../base/pageWidget/base_stateful_widget.dart';
import 'nodes_logic.dart';
import 'nodes_state.dart';

class NodesPage extends BaseStatefulWidget<NodesLogic, NodesState> {
  const NodesPage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context) {
    return Container(
      child: Center(
        child: ElevatedButton(
          onPressed: () {
            // 在按钮点击时执行的操作
            Get.back();
          },
          child: Text('Click back'),
        ),
      ),
    );
  }

  @override
  String titleString() => "节点列表";

  @override
  bool showBackButton() => false;

  @override
  bool showTitleBar() => true;
}
