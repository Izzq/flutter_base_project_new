import 'package:base_project/common/utils/log_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../base/pageWidget/base_stateful_widget.dart';
import 'test_logic.dart';
import 'test_state.dart';

class TestPage extends BaseStatefulWidget<TestLogic, TestState> {
  const TestPage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context) {
    return Container(
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () {
              // 在按钮点击时执行的操作
              controller.increment();
            },

            ///需要结合 update(); 使用
            child: GetBuilder<TestLogic>(
                builder: (_) => Text(
                      'GetBuilder clicks: ${state.count.value}',
                    )),
          ),
          ElevatedButton(
            onPressed: () {
              // 在按钮点击时执行的操作
              controller.increment();
            },
            child: GetX<TestLogic>(
                builder: (_) => Text(
                      'GetX clicks: ${state.count.value}',
                    )),
          ),
          ElevatedButton(
            onPressed: () {
              // 在按钮点击时执行的操作
              controller.increment();
            },
            child: Obx(() => Text(
                  'Obx clicks: ${state.count.value}',
                )),
          ),
          ElevatedButton(
            onPressed: _updateAlbum,
            child: Text(
              '调用原生方法 _updateAlbum',
            ),
          ),
          ElevatedButton(
            onPressed: _requestPermission,
            child: Text(
              '调用原生方法 _requestPermission',
            ),
          ),
          ElevatedButton(
            onPressed: _stop,
            child: Text(
              '调用原生方法 stop',
            ),
          ),
          ElevatedButton(
            onPressed: _goDDDDDD,
            child: Text(
              '调用原生方法 _goDDDDDD',
            ),
          ),
        ],
      ),
    );
  }

  // 创建MethodChannel
  static const platform =
      const MethodChannel('com.hu.pro/com.won.project.MethodPlugin');

  // 调用原生获取电量信息的方法
  Future<Null> _goDDDDDD() async {
    try {
      // 通过invokeMethod进行反射调用获取电量的原生中定义的方法名，获取返回值
      final String result = await platform.invokeMethod(
          '_goDDDDDD', {'param1': "11111111", 'param2': "22222222"});

      LogD("[Flutter] result _goDDDDDD: ${result}");
    } on Exception catch (e) {
      LogD("[Flutter] Exception _goDDDDDD: ${e}");
    }
  }

  // 调用原生获取电量信息的方法
  Future<Null> _stop() async {
    try {
      // 通过invokeMethod进行反射调用获取电量的原生中定义的方法名，获取返回值
      final String result = await platform
          .invokeMethod('stop', {'param1': "11111111", 'param2': "22222222"});

      LogD("[Flutter] result stop: ${result}");
    } on PlatformException catch (e) {
      LogD("[Flutter] Exception stop: ${e.message}");
    }
  }

  Future<Null> _updateAlbum() async {
    try {
      // 通过invokeMethod进行反射调用获取电量的原生中定义的方法名，获取返回值
      final String result = await platform.invokeMethod(
          'updateAlbum', {'param1': "11111111", 'param2': "22222222"});

      LogD("[Flutter] result _getBatteryLevel: ${result}");
    } on PlatformException catch (e) {
      LogD("[Flutter] Exception _getBatteryLevel: ${e.message}");
    }
  }

  // 前面的步骤相同，就不重复，直接写Flutter逻辑
  // 判断是否有权限，无权限就主动申请权限
  Future<Null> _requestPermission() async {
    bool hasPermission;
    try {
      // 传递参数，key-value形式
      hasPermission =
          await platform.invokeMethod('requestPermission', <String, dynamic>{
        'permissionName': 'WRITE_EXTERNAL_STORAGE',
        'permissionId': 0,
      });

      LogD("[Flutter] _requestPermission: ${hasPermission}");
    } on PlatformException catch (e) {
      hasPermission = false;
    }

    // setState(() {
    //   _hasPermission = hasPermission;
    // });
  }

  @override
  String titleString() => "测试页";

  @override
  bool showBackButton() => false;

  @override
  bool showTitleBar() => true;
}
