import 'dart:async';
import 'dart:developer' as developer;

import 'package:base_project/common/utils/log_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../platform/demo/enums.dart';
import '../../platform/demo/flutter_plus.dart';

class ConnectStatusPage extends StatefulWidget {
  const ConnectStatusPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ConnectStatusPage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ConnectStatusPage> {
  FlutterResult _connectionStatus = FlutterResult.none;
  final FlutterBridgeImpl _connectivity = FlutterBridgeImpl();
  late StreamSubscription<FlutterResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late FlutterResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(FlutterResult result) async {
    LogD("_updateConnectionStatus aaaa ----- >> ${result}");
    setState(() {
      _connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('FlutterBridgeImpl example app'),
        elevation: 4,
      ),
      body: Center(
          child: Text('Connection Status: ${_connectionStatus.toString()}')),
    );
  }
}
