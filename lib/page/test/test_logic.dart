import '../../base/controller/base_controller.dart';
import 'test_state.dart';

class TestLogic extends BaseController {
  final TestState state = TestState();

  void increment() {
    state.count++;
    // update();
  }
}
