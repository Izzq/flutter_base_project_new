import 'package:base_project/route/router_utils.dart';
import 'package:flutter/material.dart';

import '../../base/pageWidget/base_stateful_widget.dart';
import 'splash_logic.dart';
import 'splash_state.dart';

class SplashPage extends BaseStatefulWidget<SplashLogic, SplashState> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context) {
    return Container(
      child: Center(
        child: ElevatedButton(
          onPressed: () {
            // 在按钮点击时执行的操作
            RouterUtils.goMainPage();
          },
          child: Text('Click Me to mainPage'),
        ),
      ),
    );
  }

  @override
  String titleString() => "启动页";

  @override
  bool showBackButton() => false;

  @override
  bool showTitleBar() => true;
}
