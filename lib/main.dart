import 'dart:async';

import 'package:base_project/app.dart';
import 'package:base_project/env/config_wrapper.dart';
import 'package:base_project/env/env_config.dart';
import 'package:base_project/page/error_page.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'common/utils/log_utils.dart';
import 'env/dev.dart';
import 'global/global_controller.dart';
import 'global/global_service.dart';

///TODO: 测试环境
void main() {
  runZonedGuarded(() {
    ErrorWidget.builder = (FlutterErrorDetails details) {
      Zone.current.handleUncaughtError(details.exception, details.stack!);

      ///此处仅为展示，正规的实现方式参考 _defaultErrorWidgetBuilder 通过自定义 RenderErrorBox 实现
      return ErrorPage(
          details.exception.toString() + "\n " + details.stack.toString(),
          details);
    };

    Get.put(GlobalController());
    Get.put(GlobalService());

    ///运行应用
    runApp(ConfigWrapper(
      child: FlutterApp(),
      config: EnvConfig.fromJson(config),
    ));

    ///屏幕刷新率和显示率不一致时的优化，必须挪动到 runApp 之后
    GestureBinding.instance.resamplingEnabled = true;
  }, (Object obj, StackTrace stack) {
    logObject(obj);
    logObject(stack);
  });
}
