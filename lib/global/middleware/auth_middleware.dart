import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../route/routes.dart';
import '../global_controller.dart';

class AuthMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    // 获取用户登录状态，这里可以根据你的具体实现方式来获取
    var globalController = GetInstance().find<GlobalController>();
    // 从你的身份验证服务中获取用户登录状态
    bool isLoggedIn = globalController.state.login ?? false;

    if (!isLoggedIn) {
      // 用户未登录，跳转到登录页面
      Future.delayed(Duration(seconds: 1), () => Get.snackbar("提示", "请先登录APP"));
      return RouteSettings(name: AppRoutes.loginPage);
    }

    return null;
  }
}
