import 'package:base_project/global/global_state.dart';
import 'package:get/get.dart';

class GlobalController extends GetxController {
  final GlobalState state = GlobalState();
}
