import 'package:base_project/model/User.dart';
import 'package:flutter/material.dart';
import 'middleware/auth_middleware.dart';

/**
 * 全局State
 */
class GlobalState {
  ///用户信息
  User? userInfo;

  ///主题数据
  ThemeData? themeData;

  ///语言
  Locale? locale;

  ///当前手机平台默认语言
  Locale? platformLocale;

  ///是否登录
  bool? login;

  ///构造方法
  GlobalState({this.userInfo, this.themeData, this.locale, this.login});
}

final middlewares = [AuthMiddleware()];
