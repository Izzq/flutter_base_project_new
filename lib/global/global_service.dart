import 'package:base_project/global/global_state.dart';
import 'package:get/get.dart';

class GlobalService extends GetxService {
  final GlobalState state = GlobalState();
}
