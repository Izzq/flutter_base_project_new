package com.won.project.bridge.channel

import com.won.project.bridge.channel.PlatformConst
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler


/**
 * 接收flutter平台方法渠道实现
 *
 * 作用：接收flutter平台发送过来的事件，根据 MethodCall.method 处理不同的动作，同时给于对应的回应。
 */
class AndroidMethodCallHandlerImpl : MethodCallHandler {

    override fun onMethodCall(methodCall: MethodCall, result: MethodChannel.Result) {
        when (methodCall.method) {
            PlatformConst.ACTION_VPN_CONNECT -> {
                //根据方法名称，执行相对动作
                result.success(true)//成功回调
            }

            PlatformConst.ACTION_VPN_DISCONNECT -> {
                //根据方法名称，执行相对动作
                result.success(true)//成功回调
            }

            PlatformConst.ACTION_VPN_SWITCH_CONNECT -> {
                //根据方法名称，执行相对动作
                result.success(true)//成功回调
            }

            else -> {
                //未实现的方法。
                result.notImplemented()
            }
        }
    }
}