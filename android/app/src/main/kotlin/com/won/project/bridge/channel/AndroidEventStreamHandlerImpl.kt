package com.won.project.bridge.channel

import io.flutter.plugin.common.EventChannel


/**
 * 发送flutter平台方法事件实现
 *
 * 作用：给flutter平台发送事件使用。
 */
class AndroidEventStreamHandlerImpl : EventChannel.StreamHandler {

    // 事件派发对象
    private var eventSink: EventChannel.EventSink? = null

    override fun onListen(arguments: Any, events: EventChannel.EventSink) {
        eventSink = events
    }

    override fun onCancel(arguments: Any) {
        eventSink = null
    }

    fun sendResult(arguments: Any) {
        eventSink?.success(arguments)
    }

    fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
        eventSink?.error(errorCode, errorMessage, errorDetails)
    }

}