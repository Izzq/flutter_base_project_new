package com.won.project.bridge.channel

object PlatformConst {
    const val MethodChannel = "com.won.project/MethodChannel"
    const val EventChannel = "com.won.project/EventChannel"

    ///////////////////////////////////////////////////////


    const val ACTION_VPN_CONNECT = "action_vpn_connect"
    const val ACTION_VPN_DISCONNECT = "action_vpn_disconnect"
    const val ACTION_VPN_SWITCH_CONNECT = "action_vpn_switch_connect"

    ///////////////////////////////////////////////////////
    const val UPDATE_VPN_STATE = "update_vpn_state"
    const val UPDATE_CONFIG = "update_config"
}