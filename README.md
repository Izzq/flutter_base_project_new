## 项目目录结构

### [base](base)：逻辑层和view层的基类

### [common](common)：通用目录

### [db](db)：数据库相关

### [env](env)：应用灰度/正式环境配置

### [ext](ext)：扩展类

### [mixin](mixin)：扩展类

### [model](model)：数据实体bean

### [page](page)：UI页面

### [platform](platform)：多平交互，建立一套多平台通用的协议。

### [global](global)：使用 getX状态管理的应用全局状态管理（打算使用getX或者其他方式替换）

### [res](res)：字符串、颜色、样式、主题相关

### [route](route)：应用路由管理（页面跳转）

### [widget](widget)：自定义组件（自定义View）

### [app.dart](app.dart)：Application（flutter应用，跟Android的Application不为同一个）

### [main.dart](main.dart)：测试环境启动类

### [main_prod.dart](main_prod.dart)：正式环境启动类

------------------------------------------------------------------------------------------------

### [common](common)：通用目录

#### [config](common%2Fconfig)：常量、工程通用配置

#### [dao](common%2Fdao)：数据库dao层（考虑是否需要合并网络访问层一起处理在此层）

#### [event](common%2Fevent)：应用事件总线

#### [net](common%2Fnet)：网络请求相关（拦截器、api接口等）

#### [utils](common%2Futils)：工具

------------------------------------------------------------------------------------------------

### 工程公共布局

#### [loading_widget.dart](widget%2Floading_widget.dart)：公共 空布局、AppBar、状态栏、异常布局1

#### [error_page.dart](page%2Ferror_page.dart)[loading_widget.dart](widget%2Floading_widget.dart)：异常布局2






